<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }

    table, th, td {
        border: 1px solid black;
    }

    th, td {
        padding: 8px;
        text-align: left;
    }
</style>

<a href="{{ route('gasStations.create') }}">Create Zapravka</a> <br>

@if(count($gasStations) > 0)
    <table>
        <thead>
        <tr>
            <th>Fullname</th>
            <th>Address</th>
            <th>Fuel</th>
            <th>Fuel Price</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($gasStations as $gasStation)
            <tr>

                <td>{{ $gasStation->fullname }}</td>
                <td>{{ $gasStation->address }}</td>
                <td>{{ $gasStation->fuel }}</td>
                <td>{{ $gasStation->fuelprice }}</td>
                <td>

                    <form action="{{ route('gasStations.destroy', $gasStation->id) }}" method="post" style="display: inline;">
                        @csrf
                        @method('DELETE')
                        <button type="submit">Delete</button>
                    </form>

                    <a href="{{ route('gasStations.edit', $gasStation->id) }}">Edit</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    <p>No data available.</p>
@endif
