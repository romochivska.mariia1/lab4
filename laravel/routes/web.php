<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\gasStationsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/gasStations/create', [gasStationsController::class, 'create'])->name('gasStations.create');
    Route::post('/gasStations', [gasStationsController::class, 'store'])->name('gasStations.store');
    Route::get('/gasStations/{gasStations}', [gasStationsController::class, 'show'])->name('gasStations.show');
    Route::get('/gasStations/{gasStations}/edit', [gasStationsController::class, 'edit'])->name('gasStations.edit');
    Route::put('/gasStations/{gasStations}', [gasStationsController::class, 'update'])->name('gasStations.update');
    Route::delete('/gasStations/{gasStations}', [gasStationsController::class, 'destroy'])->name('gasStations.destroy');
});

Route::get('/gasStations', [gasStationsController::class, 'index'])->name('gasStations.index');

require __DIR__.'/auth.php';
