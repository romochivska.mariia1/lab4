<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('gasStations', function (Blueprint $table) {
            $table->id();
            $table->string('address', 200);
            $table->string('fullname', 200);
            $table->double('fuel', 200,2);
            $table->double('fuelprice', 10, 2);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('gasStations');
    }
};
