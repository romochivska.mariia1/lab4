<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class  gasStation extends Model
{
    use HasFactory;

    protected $fillable = ['address', 'fullname', 'fuel', 'fuelprice', 'creator_user_id'];

    public $timestamps = true;
}
