<?php

namespace App\Http\Controllers;

use App\Models\gasStation;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;


class gasStationsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $gasStations = gasStation::all();
        return view('gasStations.index', compact('gasStations'));
    }

    public function create()
    {
        $user = Auth::user();
        if(Gate::forUser($user)->allows('create-gasStation')){
            return view('gasStations.create');
        }else
        {
            abort(403);
        }

    }

    public function store(Request $request)
    {
        $request->validate([
            'fullname' => 'required',
            'address' => 'required',
            'fuel' => 'required',
            'fuelprice' => 'required',
        ]);
        $user = Auth::user();
        gasStation::create(array_merge($request->all(), ['creator_user_id' => $user->id]));

        return redirect()->route('gasStations.index');
    }


    public function show(gasStation $gasStations)
    {
        $user = Auth::user();
        if(Gate::forUser($user)->allows('create-gasStation')){
            return view('gasStations.show', compact('gasStations'));
        } else{
            abort(403);
        }

    }

    public function edit($id)
    {
        $user = Auth::user();
        $gasStations = gasStation::findOrFail($id);
        if(Gate::forUser($user)->allows('edit-gasStation',$gasStations)){

            return view('gasStations.edit', compact('gasStations'));
        } else {
            abort(403);
        }

    }

    public function update(Request $request, gasStation $gasStations)
    {

        $request->validate([
            'fullname' => 'required',
            'address' => 'required',
            'fuel' => 'required',
            'fuelprice' => 'required',
        ]);


        $gasStations->update([
            'fullname' => $request->input('fullname'),
            'address' => $request->input('address'),
            'fuel' => $request->input('fuel'),
            'fuelprice' => $request->input('fuelprice'),
        ]);

        return redirect()->route('gasStations.index');
    }



    public function destroy($id)
    {
        $user = Auth::user();
        $gasStation = gasStation::findOrFail($id);
        if(Gate::forUser($user)->allows('delete-gasStation',$gasStation)){
            $gasStation->delete();
            return redirect('/gasStations')->with('success','Zapravka delete successfully');
        }else
        {
            abort(403);
        }


    }
}
