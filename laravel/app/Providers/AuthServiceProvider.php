<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

use Illuminate\Support\Facades\Gate;
use App\Models\User;
class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        //
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        Gate::define('create-gasStation', function (User $user) {
            return true;
        });
        Gate::define('show-gasStation', function (User $user) {
            return true;
        });
        Gate::define('delete-gasStation', function (User $user) {
            return true;
        });

       // Gate::define('edit-gasStation', function (User $user, $gasStation) {
            // if($user->role === "editor"||$user->role === "superadmin") return true;
            //return $user->id === $gasStation->creator_user_id;
       // });
        Gate::define('edit-gasStation', function (User $user, $gasStation) {
            return $user->id === $gasStation->creator_user_id;
        });
        //Gate::define('delete-gasStation', function (User $user, $gasStation) {
          //if($user->role === "superadmin") return true;
        //return $user->id === $gasStation->user_id;
        //});

    }
}
